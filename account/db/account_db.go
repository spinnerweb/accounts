package db

import (
	"context"
	"errors"
	"log"

	"bitbucket.org/spinnerweb/accounts/db"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// AccountType type of account
type AccountType string

const (
	// MoneyAccount ...
	MoneyAccount AccountType = "moneyAccount"
	// Asset ...
	Asset AccountType = "asset"
	// IncomeExpense ...
	IncomeExpense AccountType = "incomeExpense"
)

// Account - Model of a basic account
type Account struct {
	ID       primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	Name     string             `json:"name"`
	Source   string             `json:"source"`
	Type     AccountType        `json:"type"`
	Currency string             `json:"currency"`
	Parent   primitive.ObjectID `json:"parent"`
}

// AccountCollection ...
type AccountCollection struct {
	*mongo.Client
	*mongo.Collection
	context.Context
	context.CancelFunc
}

// GetAccountCollection ...
func GetAccountCollection() AccountCollection {
	client, ctx, cancel := db.GetConnection()
	db := client.Database(db.GetDatabaseName())
	collection := db.Collection("accounts")

	return AccountCollection{
		Client:     client,
		Collection: collection,
		Context:    ctx,
		CancelFunc: cancel,
	}
}

// Disconnect ...
func (collection *AccountCollection) Disconnect() {
	collection.CancelFunc()
	collection.Client.Disconnect(collection.Context)
}

// GetAllAccounts Retrives all accounts from the db
func (collection *AccountCollection) GetAllAccounts() ([]*Account, error) {
	var accounts []*Account = []*Account{}
	ctx := collection.Context

	cursor, err := collection.Collection.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	err = cursor.All(ctx, &accounts)
	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return accounts, nil
}

// GetAccountByID Retrives a account by its id from the db
func (collection *AccountCollection) GetAccountByID(id string) (*Account, error) {
	var account *Account
	ctx := collection.Context

	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Printf("Failed parsing id %v", err)
		return nil, err
	}
	result := collection.Collection.FindOne(ctx, bson.D{bson.E{Key: "_id", Value: objID}})
	if result == nil {
		return nil, errors.New("Could not find a Account")
	}
	err = result.Decode(&account)

	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return account, nil
}

// GetAccountByIDs Retrives accounts by their ids from the db
func (collection *AccountCollection) GetAccountByIDs(ids *[]primitive.ObjectID) (*[]Account, error) {
	ctx := collection.Context
	var accounts []Account = []Account{}
	cursor, err := collection.Collection.Find(ctx, bson.D{bson.E{Key: "_id", Value: bson.M{"$in": ids}}})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	err = cursor.All(ctx, &accounts)
	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return &accounts, nil

}

// Create creating a account in a mongo
func (collection *AccountCollection) Create(account *Account) (primitive.ObjectID, error) {
	ctx := collection.Context
	account.ID = primitive.NewObjectID()

	result, err := collection.Collection.InsertOne(ctx, account)
	if err != nil {
		log.Printf("Could not create Account: %v", err)
		return primitive.NilObjectID, err
	}
	oid := result.InsertedID.(primitive.ObjectID)
	return oid, nil
}

// CreateMany creating many accounts in a mongo
func (collection *AccountCollection) CreateMany(accounts *[]Account) (*[]primitive.ObjectID, error) {
	ctx := collection.Context

	var ui []interface{}
	for _, t := range *accounts {
		t.ID = primitive.NewObjectID()
		ui = append(ui, t)
	}

	result, err := collection.Collection.InsertMany(ctx, ui)
	if err != nil {
		log.Printf("Could not create Account: %v", err)
		return nil, err
	}

	var oids = []primitive.ObjectID{}

	for _, id := range result.InsertedIDs {
		oids = append(oids, id.(primitive.ObjectID))
	}
	return &oids, nil
}

//Update updating an existing account in a mongo
func (collection *AccountCollection) Update(account *Account) (*Account, error) {
	ctx := collection.Context
	var updatedAccount *Account

	update := bson.M{
		"$set": account,
	}

	upsert := true
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		Upsert:         &upsert,
		ReturnDocument: &after,
	}

	err := collection.Collection.FindOneAndUpdate(ctx, bson.M{"_id": account.ID}, update, &opt).Decode(&updatedAccount)
	if err != nil {
		log.Printf("Could not save Account: %v", err)
		return nil, err
	}
	return updatedAccount, nil
}

// DeleteAccountByID Deletes an account by its id from the db
func (collection *AccountCollection) DeleteAccountByID(id string) error {
	ctx := collection.Context
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Printf("Failed parsing id %v", err)
		return err
	}
	result, err := collection.Collection.DeleteOne(ctx, bson.D{bson.E{Key: "_id", Value: objID}})
	if result == nil {
		return errors.New("Could not find a Account")
	}

	if err != nil {
		log.Printf("Failed deleting %v", err)
		return err
	}
	return nil
}
