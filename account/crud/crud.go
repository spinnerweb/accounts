package crud

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	db "bitbucket.org/spinnerweb/accounts/account/db"
)

func handleGetAccounts(c *gin.Context) {
	collection := db.GetAccountCollection()
	defer collection.Disconnect()

	var loadedAccounts, err = collection.GetAllAccounts()
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"accounts": loadedAccounts})
}

func handleGetAccount(c *gin.Context) {
	collection := db.GetAccountCollection()
	defer collection.Disconnect()
	id := c.Param("id")
	var loadedAccount, err = collection.GetAccountByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, loadedAccount)
}

func handleCreateAccount(c *gin.Context) {
	collection := db.GetAccountCollection()
	defer collection.Disconnect()

	var account db.Account
	if err := c.ShouldBindJSON(&account); err != nil {
		log.Print(err)
		c.JSON(http.StatusBadRequest, gin.H{"msg": err})
		return
	}

	missingFields := []string{}
	if account.Name == "" {
		missingFields = append(missingFields, "name")
	}
	if account.Type == "" {
		missingFields = append(missingFields, "type")
	}

	if len(missingFields) > 0 {
		c.JSON(http.StatusBadRequest, gin.H{"msg": fmt.Sprintf("Missing fields: %v", missingFields)})
		return
	}

	id, err := collection.Create(&account)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"id": id})
}

func handleUpdateAccount(c *gin.Context) {
	collection := db.GetAccountCollection()
	defer collection.Disconnect()

	var account db.Account
	if err := c.ShouldBindJSON(&account); err != nil {
		log.Print(err)
		c.JSON(http.StatusBadRequest, gin.H{"msg": err})
		return
	}
	savedAccount, err := collection.Update(&account)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"account": savedAccount})
}

func handleDeleteAccount(c *gin.Context) {
	collection := db.GetAccountCollection()
	defer collection.Disconnect()
	id := c.Param("id")
	var err = collection.DeleteAccountByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.Status(http.StatusNoContent)
}

// Setup Setup REST API
func Setup(r *gin.Engine) {
	r.GET("/accounts/:id", handleGetAccount)
	r.GET("/accounts", handleGetAccounts)
	r.POST("/accounts", handleCreateAccount)
	r.PUT("/accounts", handleUpdateAccount)
	r.DELETE("/accounts/:id", handleDeleteAccount)
}
