package crud

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"bitbucket.org/spinnerweb/accounts/db"
	"bitbucket.org/spinnerweb/accounts/server"
)

func TestGetAccounts(t *testing.T) {

	server := server.Configure()
	Setup(server)
	ts := httptest.NewServer(server)
	defer ts.Close()

	resp, err := http.Get(fmt.Sprintf("%s/accounts", ts.URL))

	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != 200 {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}

	val, ok := resp.Header["Content-Type"]

	// Assert that the "content-type" header is actually set
	if !ok {
		t.Fatalf("Expected Content-Type header to be set")
	}

	// Assert that it was set as expected
	if val[0] != "application/json; charset=utf-8" {
		t.Fatalf("Expected \"application/json; charset=utf-8\", got %s", val[0])
	}

}

func TestGetAccountNotFound(t *testing.T) {
	server := server.Configure()
	Setup(server)
	ts := httptest.NewServer(server)
	defer ts.Close()

	resp, err := http.Get(fmt.Sprintf("%s/account/%s", ts.URL, "unknown"))

	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != 404 {
		t.Fatalf("Expected status code 404, got %v", resp.StatusCode)
	}
}

func TestGetAccountFullCRUD(t *testing.T) {
	name := "test-account"
	id := testCreateAccount(t, name)
	testGetAccount(t, id, name)
}

func testCreateAccount(t *testing.T, name string) string {
	server := server.Configure()
	Setup(server)
	ts := httptest.NewServer(server)
	defer ts.Close()

	requestBody, _ := json.Marshal(map[string]string{
		"name": name,
	})

	resp, err := http.Post(fmt.Sprintf("%s/accounts", ts.URL), "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != 200 {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}

	val, ok := resp.Header["Content-Type"]

	// Assert that the "content-type" header is actually set
	if !ok {
		t.Fatalf("Expected Content-Type header to be set")
	}

	// Assert that it was set as expected
	if val[0] != "application/json; charset=utf-8" {
		t.Fatalf("Expected \"application/json; charset=utf-8\", got %s", val[0])
	}

	defer resp.Body.Close()

	var res db.Account
	json.NewDecoder(resp.Body).Decode(&res)

	return res.ID.Hex()
}

func testGetAccount(t *testing.T, id string, name string) string {
	server := server.Configure()
	Setup(server)
	ts := httptest.NewServer(server)
	defer ts.Close()

	resp, err := http.Get(fmt.Sprintf("%s/accounts/%s", ts.URL, id))
	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != 200 {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}

	val, ok := resp.Header["Content-Type"]

	// Assert that the "content-type" header is actually set
	if !ok {
		t.Fatalf("Expected Content-Type header to be set")
	}

	// Assert that it was set as expected
	if val[0] != "application/json; charset=utf-8" {
		t.Fatalf("Expected \"application/json; charset=utf-8\", got %s", val[0])
	}

	defer resp.Body.Close()

	var res db.Account
	json.NewDecoder(resp.Body).Decode(&res)

	assert.Equal(t, name, res.Name)

	return res.ID.Hex()
}
