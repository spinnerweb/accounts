FROM golang:1.14.3-alpine AS build
WORKDIR /src/
COPY . /src/
RUN unset GOPATH && go build -o /bin/accounts .
ENTRYPOINT  ["/bin/accounts"]