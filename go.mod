module bitbucket.org/spinnerweb/accounts

go 1.15

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/stretchr/testify v1.4.0
	go.mongodb.org/mongo-driver v1.4.1
)
