package main

import (
	account "bitbucket.org/spinnerweb/accounts/account/crud"
	balance "bitbucket.org/spinnerweb/accounts/balance/crud"
	"bitbucket.org/spinnerweb/accounts/server"
)

func main() {
	server := server.Configure()
	account.Setup(server)
	balance.Setup(server)
	server.Run("0.0.0.0:4001") // listen and serve on 0.0.0.0:8080
}
