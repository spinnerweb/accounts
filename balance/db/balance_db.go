package db

import (
	"context"
	"errors"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"bitbucket.org/spinnerweb/accounts/db"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Balance - Model of a basic balance
type Balance struct {
	ID        primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	AccountID primitive.ObjectID `json:"accountId"`
	Date      time.Time          `json:"date"`
	Amount    float64            `json:"amount"`
}

// BalanceCollection ...
type BalanceCollection struct {
	*mongo.Client
	*mongo.Collection
	context.Context
	context.CancelFunc
}

// GetBalanceCollection ...
func GetBalanceCollection() BalanceCollection {
	client, ctx, cancel := db.GetConnection()
	db := client.Database(db.GetDatabaseName())
	collection := db.Collection("balances")

	return BalanceCollection{
		Client:     client,
		Collection: collection,
		Context:    ctx,
		CancelFunc: cancel,
	}
}

// Disconnect ...
func (collection *BalanceCollection) Disconnect() {
	collection.CancelFunc()
	collection.Client.Disconnect(collection.Context)
}

// GetAllBalances Retrives all balances from the db
func (collection *BalanceCollection) GetAllBalances() ([]*Balance, error) {
	var balances []*Balance = []*Balance{}
	ctx := collection.Context

	cursor, err := collection.Collection.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	err = cursor.All(ctx, &balances)
	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return balances, nil
}

// GetBalanceByID Retrives a balance by its id from the db
func (collection *BalanceCollection) GetBalanceByID(id string) (*Balance, error) {
	var balance *Balance
	ctx := collection.Context

	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Printf("Failed parsing id %v", err)
		return nil, err
	}
	result := collection.Collection.FindOne(ctx, bson.D{bson.E{Key: "_id", Value: objID}})
	if result == nil {
		return nil, errors.New("Could not find a Balance")
	}
	err = result.Decode(&balance)

	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return balance, nil
}

// GetBalanceByAccountID Retrives the balances for a given account, ordered by date ascending
func (collection *BalanceCollection) GetBalanceByAccountID(id string) ([]*Balance, error) {
	objID, err := primitive.ObjectIDFromHex(id)
	ctx := collection.Context
	findOptions := options.Find()
	findOptions.SetSort(bson.D{bson.E{Key: "date", Value: 1}})

	var balances []*Balance = []*Balance{}
	cursor, err := collection.Collection.Find(ctx, bson.D{bson.E{Key: "accountid", Value: objID}}, findOptions)
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	err = cursor.All(ctx, &balances)
	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return balances, nil
}

// GetBalanceByIDs Retrives balances by their ids from the db
func (collection *BalanceCollection) GetBalanceByIDs(ids *[]primitive.ObjectID) (*[]Balance, error) {
	ctx := collection.Context
	var balances []Balance = []Balance{}
	cursor, err := collection.Collection.Find(ctx, bson.D{bson.E{Key: "_id", Value: bson.M{"$in": ids}}})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	err = cursor.All(ctx, &balances)
	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return &balances, nil

}

// Create creating a balance in a mongo
func (collection *BalanceCollection) Create(balance *Balance) (primitive.ObjectID, error) {
	ctx := collection.Context
	balance.ID = primitive.NewObjectID()

	result, err := collection.Collection.InsertOne(ctx, balance)
	if err != nil {
		log.Printf("Could not create Balance: %v", err)
		return primitive.NilObjectID, err
	}
	oid := result.InsertedID.(primitive.ObjectID)
	return oid, nil
}

// CreateMany creating many balances in a mongo
func (collection *BalanceCollection) CreateMany(balances *[]Balance) (*[]primitive.ObjectID, error) {
	ctx := collection.Context

	var ui []interface{}
	for _, t := range *balances {
		t.ID = primitive.NewObjectID()
		ui = append(ui, t)
	}

	result, err := collection.Collection.InsertMany(ctx, ui)
	if err != nil {
		log.Printf("Could not create Balance: %v", err)
		return nil, err
	}

	var oids = []primitive.ObjectID{}

	for _, id := range result.InsertedIDs {
		oids = append(oids, id.(primitive.ObjectID))
	}
	return &oids, nil
}

// ReplaceAll first delete all and then create many balances in a mongo db
func (collection *BalanceCollection) ReplaceAll(balances *[]Balance) (*[]primitive.ObjectID, error) {
	ctx := collection.Context

	err := collection.Collection.Drop(ctx)
	if err != nil {
		return nil, err
	}

	var ui []interface{}
	for _, t := range *balances {
		t.ID = primitive.NewObjectID()
		ui = append(ui, t)
	}

	result, err := collection.Collection.InsertMany(ctx, ui)
	if err != nil {
		log.Printf("Could not create Balance: %v", err)
		return nil, err
	}

	var oids = []primitive.ObjectID{}

	for _, id := range result.InsertedIDs {
		oids = append(oids, id.(primitive.ObjectID))
	}
	return &oids, nil
}

//Update updating an existing balance in a mongo
func (collection *BalanceCollection) Update(balance *Balance) (*Balance, error) {
	ctx := collection.Context
	var updatedBalance *Balance

	update := bson.M{
		"$set": balance,
	}

	upsert := true
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		Upsert:         &upsert,
		ReturnDocument: &after,
	}

	err := collection.Collection.FindOneAndUpdate(ctx, bson.M{"_id": balance.ID}, update, &opt).Decode(&updatedBalance)
	if err != nil {
		log.Printf("Could not save Balance: %v", err)
		return nil, err
	}
	return updatedBalance, nil
}

// DeleteBalanceByID Deletes an balance by its id from the db
func (collection *BalanceCollection) DeleteBalanceByID(id string) error {
	ctx := collection.Context
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Printf("Failed parsing id %v", err)
		return err
	}
	result, err := collection.Collection.DeleteOne(ctx, bson.D{bson.E{Key: "_id", Value: objID}})
	if result == nil {
		return errors.New("Could not find a Balance")
	}

	if err != nil {
		log.Printf("Failed deleting %v", err)
		return err
	}
	return nil
}
