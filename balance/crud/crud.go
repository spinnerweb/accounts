package crud

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	db "bitbucket.org/spinnerweb/accounts/balance/db"
)

func handleGetBalances(c *gin.Context) {
	collection := db.GetBalanceCollection()
	defer collection.Disconnect()

	var loadedBalances, err = collection.GetAllBalances()
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"balances": loadedBalances})
}

func handleGetBalance(c *gin.Context) {
	collection := db.GetBalanceCollection()
	defer collection.Disconnect()
	id := c.Param("id")
	var loadedBalance, err = collection.GetBalanceByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, loadedBalance)
}

func handleGetAccountBalances(c *gin.Context) {
	collection := db.GetBalanceCollection()
	defer collection.Disconnect()
	accountID := c.Param("id")
	var loadedBalance, err = collection.GetBalanceByAccountID(accountID)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, loadedBalance)
}

func handleCreateBalance(c *gin.Context) {
	collection := db.GetBalanceCollection()
	defer collection.Disconnect()

	var balance db.Balance
	if err := c.ShouldBindJSON(&balance); err != nil {
		log.Print(err)
		c.JSON(http.StatusBadRequest, gin.H{"msg": err})
		return
	}
	id, err := collection.Create(&balance)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"id": id})
}

func handleReplaceAllBalances(c *gin.Context) {
	collection := db.GetBalanceCollection()
	defer collection.Disconnect()

	var balances []db.Balance
	if err := c.ShouldBindJSON(&balances); err != nil {
		log.Print(err)
		c.JSON(http.StatusBadRequest, gin.H{"msg": err})
		return
	}
	err, oids := collection.ReplaceAll(&balances)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ids": oids})
}

func handleUpdateBalance(c *gin.Context) {
	collection := db.GetBalanceCollection()
	defer collection.Disconnect()

	var balance db.Balance
	if err := c.ShouldBindJSON(&balance); err != nil {
		log.Print(err)
		c.JSON(http.StatusBadRequest, gin.H{"msg": err})
		return
	}
	savedBalance, err := collection.Update(&balance)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"balance": savedBalance})
}

func handleDeleteBalance(c *gin.Context) {
	collection := db.GetBalanceCollection()
	defer collection.Disconnect()
	id := c.Param("id")
	var err = collection.DeleteBalanceByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.Status(http.StatusNoContent)
}

// Setup Setup REST API
func Setup(r *gin.Engine) {
	r.GET("/balances/:id", handleGetBalance)
	r.GET("/balances", handleGetBalances)
	r.GET("/accounts/:id/balances", handleGetAccountBalances)
	r.POST("/balances", handleCreateBalance)
	r.POST("/balances/replace-all", handleReplaceAllBalances)
	r.PUT("/balances", handleUpdateBalance)
	r.DELETE("/balances/:id", handleDeleteBalance)
}
